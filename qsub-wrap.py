#!/usr/bin/python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 17 11:43:25 2014

This code will run in the clusters native Python 2.4.3 environment. A PBS
script will be created and saved in pbs_in. Afterwards, this PBS script will
be submitted to the cluster's que with qsub, and will execute the defined
Python script in a Miniconda Python environment. The default environment is
called anaconda.

When the command that has to be submitted on the cluster also needs some
arguments, it will pass those arguments not belonging to qsub-wrap.py
to the execute command on the cluster (done in the PBS script).

A certain amount of assumptions is made:
* for a flag, the default value is assumed False, and when activated the
action is assumed store_true

@author: dave
"""

__author__ = "David Verelst <dave@dtu.dk>"
__license__ = "GPL-2+"

#from __future__ import division
#from __future__ import print_function

import sys
import os
import subprocess as sproc
from datetime import timedelta

# only python 2.7 and up
#from argparse import ArgumentParser
# python 2.4
from optparse import OptionParser

PBS_TEMP = """
### Standard Output
#PBS -N [jobname]
#PBS -o ./[pbs_out]/qsub-wrap_[jobname].out
### Standard Error
#PBS -e ./[pbs_out]/qsub-wrap_[jobname].err
#PBS -W umask=003
### Maximum wallclock time format HOURS:MINUTES:SECONDS
#PBS -l walltime=[walltime]
#PBS -lnodes=1:ppn=1
### Queue name
#PBS -q [queue]
### Browse to current working dir
cd $PBS_O_WORKDIR
pwd
### ===========================================================================
### run the job
[ANACONDA]
[PYTHON_REPOS]

[command] "[fpath]" [kwargs_submit]

[ANACONDA_STOP]

### ===========================================================================
### wait for jobs to finish
wait
exit
"""

PYTHON_REPOS = """
# add custom libraries to the python path
#export PYTHONPATH=/some/directory/to/a/python/package:$PYTHONPATH
"""

ANACONDA = """
export PATH=/home/python/miniconda3/bin:$PATH
# activate the custom python environment:
source activate [py_env]
"""

ANACONDA_STOP = """
# deactivate the python environment
source deactivate
"""

def args2dict():
    """
    Return all arguments as a dictionary. When the argument is only a switch,
    the value will be None. All values are strings otherwise.
    """
    allargs = sys.argv
    # first item is the file name
    vardict = {}
    keyflag = False
    for item in allargs[1:]:
        if item[:2] == '--':
            key = item[2:]
            keyflag = True
        elif item[:1] == '-':
            key = item[1:]
            keyflag = True
        else:
            value = item
            keyflag = False
        # if it contains the value already
        items = key.split('=')
        if len(items) > 1:
            key = items[0]
            value = items[1]
            vardict[key] = value
            keyflag = False
            continue
        else:
            if keyflag:
                vardict[key] = None
            else:
                vardict[key] = value
    return vardict


def addvariableargs(parser):
    """
    """
    vardict = args2dict()

    # build a list with the currently defined options
    options = []
    for opt in parser.option_list:
        # but drop the - or --
        options.extend([k.split('-')[-1] for k in opt.__str__().split('/')])

    # any argument that does not exist yet has to be defined via the argparse
    # otherwise we'll have complaints from argparse about to many arguments
    kwargs_pass = {}
    for key, value in vardict.iteritems():
        if key in options:
            continue
        # if it is only a flag, store to True
        if value is None:
            action = 'store_true'
            typ = None
        else:
            action = 'store'
            typ = 'string'
        if len(key) > 1:
            parser.add_argument('--%s' % key, type=typ, dest=key,
                                action=action, default=False)
        else:
            parser.add_argument('-%s' % key, type=typ, dest=key,
                                action=action, default=False)
        kwargs_pass[key] = value

    return kwargs_pass, parser


def submit_pbs(walltime='00:59:59', queue='xpresq', py_env='wetb_py3',
               pbs_in='pbs_in', pbs_out='pbs_out', jobname=None, command=None,
               fpath=None, dry=False, **kwargs):
    """
    Set the configurable PBS elements, and submit the pbs file.
    """

    # if the walltime is longer then an hour, force the workq
    hours, minutes, seconds = [int(k) for k in walltime.split(':')]
    dt = timedelta(hours=hours, minutes=minutes, seconds=seconds)
    dt_max = timedelta(hours=0, minutes=59, seconds=59)
    if dt > dt_max:
        queue = 'workq'

    kwargs_submit = ''
    # all other kwargs will be catched in kwargs and have to be passed on
    # kwargs does not contain any of the defiened kwargs of submit_pbs
    for key, value in kwargs.iteritems():
        if len(key) == 1:
            kwargs_submit += ' -%s %s' % (key, str(value))
        else:
            if value is None:
                kwargs_submit += ' --%s' % key
            else:
                kwargs_submit += ' --%s %s' % (key, str(value))

    if not os.path.isabs(fpath):
        fpath = os.path.join(os.getcwd(), fpath)
    fname = os.path.basename(fpath)
    if jobname is None:
        jobname = fname
    pbs_script = PBS_TEMP
    pbs_script = pbs_script.replace('[ANACONDA]', ANACONDA)
    pbs_script = pbs_script.replace('[py_env]', py_env)
    pbs_script = pbs_script.replace('[PYTHON_REPOS]', PYTHON_REPOS)
    pbs_script = pbs_script.replace('[ANACONDA_STOP]', ANACONDA_STOP)
    pbs_script = pbs_script.replace('[jobname]', jobname)
    pbs_script = pbs_script.replace('[pbs_out]', pbs_out)
    pbs_script = pbs_script.replace('[queue]', queue)
    pbs_script = pbs_script.replace('[walltime]', walltime)
    pbs_script = pbs_script.replace('[command]', command)
    pbs_script = pbs_script.replace('[fpath]', fpath)
    pbs_script = pbs_script.replace('[kwargs_submit]', kwargs_submit)

    print 'following command will be executed on the cluster:'
    print '%s %s %s' % (command, fpath, kwargs_submit)

    # make sure a pbs_in and pbs_out directory exists
    if not os.path.exists(pbs_in):
        os.makedirs(pbs_in)
    if not os.path.exists(pbs_out):
        os.makedirs(pbs_out)

    # write the pbs_script
    FILE = open('%s/%s.qsub-wrap' % (pbs_in, jobname), 'w')
    FILE.write(pbs_script)
    FILE.close()

    if dry:
        return pbs_script, []

    # and submit
    cmd = 'qsub %s/%s.qsub-wrap' % (pbs_in, jobname)
    print 'submitting to cluster as:'
    print cmd
    p = sproc.Popen(cmd, stdout=sproc.PIPE, stderr=sproc.STDOUT, shell=True)

    # p.wait() will lock the current shell until p is done
    # p.stdout.readlines() checks if there is any output, but also locks
    # the thread if nothing comes back
    stdout = p.stdout.readlines()
    for line in stdout:
        print line
    # wait until qsub is finished doing its magic
    p.wait()

    return pbs_script, stdout


if __name__ == '__main__':

    # parse the arguments, only relevant when using as a command line utility
    parser = OptionParser(usage = "%prog -c Command -f FileNameOrPath")
    parser.add_argument = parser.add_option
    parser.add_argument('-f', '--fpath', type='string', dest='fpath',
                        action='store', default=None,
                        help='python file name that should be run on cluster')
    parser.add_argument('-c', '--command', type='string', dest='command',
                        action='store', default='time python',
                        help='Executable, can be empty if fpath is an exe.')
    parser.add_argument('--py_env', type='string', dest='py_env',
                        help='name of the conda environment',
                        default='wetb_py3')
    parser.add_argument('-q', '--queue', type='string', dest='queue',
                        help='name of the PBS queue: workq or xpresq. '
                        'Defaults to workq', default='workq')
    parser.add_argument('-w', '--walltime', type='string', dest='walltime',
                        help='walltime, in HH:MM:SS. Default=02:00:00',
                        default='02:00:00')
    parser.add_argument('--dry', action='store_true', dest='dry', default=False,
                        help='dry run: do not submit pbs file.')
    parser.add_argument('--jobname', type='string', action='store',
                        dest='jobname', default=None,
                        help='PBS job name, default: python file name.')
    parser.add_argument('--pbs_in', type='string', action='store',
                        dest='pbs_in', default='pbs_in',
                        help='pbs_in directory, default: pbs_in')
    parser.add_argument('--pbs_out', type='string', action='store',
                        dest='pbs_out', default='pbs_out',
                        help='pbs_out directory, default: pbs_out')

    # get all the other arguments and add them to the help
    kwargs_pass, parser = addvariableargs(parser)

    # make sure a filename is given
    (opt, args) = parser.parse_args()
    if opt.fpath is None:
        parser.print_usage()
        sys.stderr.write("error: specify a file name/path with -f" + os.linesep)
        sys.exit(1)

    # create the PBS file based on the template
    # write and launch the pbs script with qsub
    pbs_script, stdout = submit_pbs(fpath=opt.fpath, command=opt.command,
                                    py_env=opt.py_env, queue=opt.queue,
                                    walltime=opt.walltime, dry=opt.dry,
                                    jobname=opt.jobname, pbs_in=opt.pbs_in,
                                    pbs_out=opt.pbs_out, **kwargs_pass)
