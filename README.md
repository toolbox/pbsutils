pbsutils
========

This is a collection of utility scripts for using the PBS queueing system on
a computer cluster. These scripts have currently some configuration switches
that re specific to some of the clusters we use at DTU Wind Energy. If you
are using these scripts on another cluster, you will need to proceed with
caution and you might need to make some changes here and there. First thing
on the to-do list would be to add all configuration or cluster dependent
settings into a seperate configuration file.

The documentation for `launch.py` can be found [here](docs/launch.md).



# setup wine environment
```
rm -f ./config-wine-hawc2.sh &&
wget https://gitlab.windenergy.dtu.dk/toolbox/pbsutils/raw/master/config-wine-hawc2.sh &&
chmod 777 config-wine-hawc2.sh &&
./config-wine-hawc2.sh
```
