#!/bin/bash

function wine_init_config () {

    echo "-------- remove/clean-up existing wine configuration: $WINEDIRNAME"
    rm -rf $WINEDIRNAME

    echo "-------- initialize winecfg"
    # execute winecfg to trigger init configuration, might fail to complete
    # on Jess since winefix has not been executed
    WINEARCH=$WINEARCH WINEPREFIX=$WINEDIRNAME wine winecfg

    # winefix only applies to Jess
    if [[ ${_HOSTNAME_:0:1} == "j" ]] ; then
        echo "-------- apply winefix"
        WINEARCH=$WINEARCH WINEPREFIX=$WINEDIRNAME winefix
    fi

    # create a directory on the user home drive in which executables are placed
    mkdir -p $EXE_DIR_UNIX

    echo "-------- copying HAWC2 executables from/to:"
    echo "$CP_EXE_FROM/*  $EXE_DIR_UNIX"
    rsync -a $CP_EXE_FROM/* "$EXE_DIR_UNIX"

    # add C:\bin (/home/$USER/$WINEDIRNAME/drive_c/bin/) to Windows PATH variable
    EXE_DIR_WINE="z:""$EXE_DIR_UNIX"
    echo "-------- adding $EXE_DIR_UNIX to wine system PATH"
    echo "-------- this is equivalent to $EXE_DIR_WINE in Windows terms."
    # create file with registry key name and path as the value
    # note that the text format of this file can make or brake the procedure
    # very sensitive to trailing slashes, empty line breaks, etc
    printf 'REGEDIT4\n[HKEY_CURRENT_USER\\Environment]\n"PATH"="'"$EXE_DIR_WINE"'"\n' >> ./tmp.reg
    WINEARCH=$WINEARCH WINEPREFIX=$WINEDIRNAME wine regedit ./tmp.reg

    # do not show GUI crash dialogs
    rm ./tmp.reg
    printf '[HKEY_CURRENT_USER\\Software\\Wine\\WineDbg]\n"ShowCrashDialog"=dword:00000000\n' >> ./tmp.reg
    WINEARCH=$WINEARCH WINEPREFIX=$WINEDIRNAME wine regedit ./tmp.reg
    rm ./tmp.reg

    # add an alias to .bashrc, but only if different than wine
    if [[ $WINE_ALIAS != "wine" ]] ; then
        echo "-------- adding alias $WINE_ALIAS to ~/.bashrc"
        echo "alias $WINE_ALIAS='$WINE_ENV_VARS wine'" >> ~/.bashrc
        source ~/.bashrc
    fi

    if [[ $WINEARCH == "win32" ]] ; then
        echo "-------- See if HAWC2 runs..."
        WINEARCH=$WINEARCH WINEPREFIX=$WINEDIRNAME wine hawc2-latest.exe
    fi

}

_HOSTNAME_=`hostname`

# 64-bit mode only relevant on Jess
if [[ ${_HOSTNAME_:0:1} == "j" ]] ; then
    echo
    echo "====================================================================="
    echo "-------- SETUP 64-bit wine environment in ~/.wine"
    echo "====================================================================="
    WINEARCH="win64"
    WINEDIRNAME="/home/$USER/.wine"
    WINE_ENV_VARS="WINEPREFIX=$WINEDIRNAME"
    WINE_ALIAS="wine"
    CP_EXE_FROM="/home/MET/hawc2exe/$WINEARCH"
    EXE_DIR_UNIX="/home/$USER/wine_exe/$WINEARCH/"
    wine_init_config
fi

# 32-bit mode for both Gorm and Jess
echo
echo "====================================================================="
echo "-------- SETUP 32-bit wine environment in ~/.wine32"
echo "====================================================================="
WINEARCH="win32"
WINEDIRNAME="/home/$USER/.wine32"
WINE_ENV_VARS="WINEARCH=$WINEARCH WINEPREFIX=$WINEDIRNAME"
WINE_ALIAS="wine32"
CP_EXE_FROM="/home/MET/hawc2exe/$WINEARCH"
EXE_DIR_UNIX="/home/$USER/wine_exe/$WINEARCH/"
wine_init_config

