#!/usr/bin/python

import os
import pbswrap

if __name__ == '__main__':

    #command = 'pbsnodes -l all' # | cut -c 22-35

    output = os.popen('pbsnodes -l all').readlines()
    pbsnodes, nodes = pbswrap.parse_pbsnode_lall(output)

    output = os.popen('qstat -n1').readlines()
    users, host, nodesload = pbswrap.parse_qstat_n1(output)

    pbswrap.print_node_loading(users, host, nodes, nodesload)
    pbswrap.print_dashboard(users, host, pbsnodes)
