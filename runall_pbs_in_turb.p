
### Standard Output
#PBS -N runall_turb
#PBS -o ./pbs_out/runall_turb.out
### Standard Error
#PBS -e ./pbs_out/runall_turb.err
#PBS -W umask=0003
### Maximum wallclock time format HOURS:MINUTES:SECONDS
#PBS -l walltime=3:00:00
#PBS -l nodes=1:ppn=20
### Queue name
#PBS -q windq


echo "----------------------------------------------------------------------"
echo "activate python environment wetb_py3"
source /home/python/miniconda3/bin/activate wetb_py3
echo "CHECK 2x IF wetb_py3 IS ACTIVE, IF NOT TRY AGAIN"
CMD="from distutils.sysconfig import get_python_lib;print (get_python_lib().find('/usr/lib/python'))"
ACTIVATED=`python -c "$CMD"`
if [ $ACTIVATED -eq 0 ]; then source /home/python/miniconda3/bin/activate wetb_py3;fi
ACTIVATED=`python -c "$CMD"`
if [ $ACTIVATED -eq 0 ]; then source /home/python/miniconda3/bin/activate wetb_py3;fi

echo "----------------------------------------------------------------------"
cd /scratch/$USER/$PBS_JOBID/
echo 'current working directory:'
pwd

echo "create CPU directories on the scratch disk"
mkdir -p /scratch/$USER/$PBS_JOBID/simid/
#mkdir -p /scratch/$USER/$PBS_JOBID/turb/
#mkdir -p /scratch/$USER/$PBS_JOBID/turb_meand/
#mkdir -p /scratch/$USER/$PBS_JOBID/turb_micro/
#mkdir -p /scratch/$USER/$PBS_JOBID/0/
#mkdir -p /scratch/$USER/$PBS_JOBID/1/
#mkdir -p /scratch/$USER/$PBS_JOBID/2/
#mkdir -p /scratch/$USER/$PBS_JOBID/3/
#mkdir -p /scratch/$USER/$PBS_JOBID/4/
#mkdir -p /scratch/$USER/$PBS_JOBID/5/
#mkdir -p /scratch/$USER/$PBS_JOBID/6/
#mkdir -p /scratch/$USER/$PBS_JOBID/7/
#mkdir -p /scratch/$USER/$PBS_JOBID/8/
#mkdir -p /scratch/$USER/$PBS_JOBID/9/
#mkdir -p /scratch/$USER/$PBS_JOBID/10/
#mkdir -p /scratch/$USER/$PBS_JOBID/11/
#mkdir -p /scratch/$USER/$PBS_JOBID/12/
#mkdir -p /scratch/$USER/$PBS_JOBID/13/
#mkdir -p /scratch/$USER/$PBS_JOBID/14/
#mkdir -p /scratch/$USER/$PBS_JOBID/15/
#mkdir -p /scratch/$USER/$PBS_JOBID/16/
#mkdir -p /scratch/$USER/$PBS_JOBID/17/
#mkdir -p /scratch/$USER/$PBS_JOBID/18/
#mkdir -p /scratch/$USER/$PBS_JOBID/19/

cp -r $PBS_O_WORKDIR/pbs_in_turb /scratch/$USER/$PBS_JOBID/simid/

echo "----------------------------------------------------------------------"
cd /scratch/$USER/$PBS_JOBID/simid
echo 'current working directory:'
pwd
echo "START RUNNING JOBS IN find+xargs MODE"
winefix
# run all the PBS *.p files in find+xargs mode
export LAUNCH_PBS_MODE=false
/home/MET/sysalt/bin/find pbs_in_turb/turb/ -type f -name '*.p' | sort -z

echo "number of files to be launched: "`find pbs_in_turb/turb/ -type f | wc -l`
/home/MET/sysalt/bin/find pbs_in_turb/turb/ -type f -name '*.p' -print0 | sort -z | /home/MET/sysalt/bin/xargs -0 -I{} --process-slot-var=CPU_NR -n 1 -P 20 sh {}
echo "END OF JOBS IN find+xargs MODE"

echo "----------------------------------------------------------------------"
echo 'total scratch disk usage:'
du -hs /scratch/$USER/$PBS_JOBID/


echo "----------------------------------------------------------------------"
# in case wine has crashed, kill any remaining wine servers
# caution: ALL the users wineservers will die on this node!
echo "following wineservers are still running:"
ps -u $USER -U $USER | grep wineserver
killall -u $USER wineserver
exit
